#include <istream>
#include <fstream>
#include <vector>
#include <map>
#include <string>

namespace network_flow
{

class Node {
  protected:
    int Id;                            // Integer id -- from 0 to |V| - 1.
    std::string Name;                  // Name of the node
    std::vector <int> Residual;        // Adjacency lists for the residual edges.
    double X;
    double Y;

    friend class Graph;
};

class Edge {
  protected:
    int Id;                            // Integer id -- from 0 to |E| - 1.
    std::string Name;                  // Name of the edge (for printing)
    int From;                          // The "from" node (id into Nodes vector)
    int To;                            // The "to" node (id into Nodes vector)
    int Original;                      // The original edge weight
    int Flow;                          // The flow
    int Residual;                      // The edge weight in the residual
    int Reverse_Id;                    // Id of the reverse edge.
    int Res_Index;                     // Index in the adjacency list of the residual
    friend class Graph;
};

class Graph {
  public:
    void Clear();                                       // Reset to empty
    void Reset();                                       // Reset residual to original
    void Read(std::istream &fin);
    void Write(std::ostream &fout, char what) const;    // 'R' prints residual
                                                        // 'F' prints flow
                                                        // 'O' prints original
                                                        // 'C' prints cut
    void Jgraph(std::ostream &fout, char what) const;   // Print jgraph.

    bool Do_Augmenting_Path(char how, bool print);      // 'D' for DFS
                                                        // 'M' for "Modified Dijkstra"
                                                        // 'E' for "Edmonds Karp"
    void Calculate_Max_Flow(char how);
    void Calculate_Min_Cut(char how);
    int Get_Flow() const;
    int Get_Num_Paths() const;

  protected:
    // I'm writing this so that we don't need to write any constructors or destructors.
    // In that way, copy/move constructors and assignment overloeads will work out of the box.

    int Source;                         // Id of the source node.
    int Sink;                           // Id of the sink node
    int Flow;                           // The flow, so far
    int Num_Paths;                      // The number of augmenting paths so far
    std::vector <int> Min_Cut;          // If calculated, the minimum cut
    std::vector <Node> Nodes;           // All nodes.
    std::vector <Edge> Edges;           // All edges.
    std::map <std::string, int> N_Map;  // Key = name, val = index into Nodes.
    std::map <std::string, int> E_Map;  // Key = name, val = index into edges.

    int Get_Node_By_Name(const std::string &name);
    int Create_Edge(const std::string &from, const std::string &to, int weight);
    void Create_Edge_And_Reverse(const std::string &from, const std::string &to, int weight);
    bool DFS(int node, std::vector <bool> &visited, std::vector <const Edge *> &path) const;
    void MC_DFS(int node, std::vector <bool> &visited);
};


};
