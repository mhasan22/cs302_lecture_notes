#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include "network_flow.hpp"
using namespace std;
using namespace network_flow;

typedef runtime_error SRE;

void print_commands(FILE *f)
{
  fprintf(f, "R file                 - Read from a file\n");
  fprintf(f, "W file FORC            - Write to a file (or - to stdout), FORC is what to write:\n");
  fprintf(f, "J file FORC            - Write jgraph\n");
  fprintf(f, "                         Flow, Original, Residual, Cut\n");
  fprintf(f, "PR                     - Print the residual graph to stdout\n");
  fprintf(f, "P                      - Print the number of paths\n");
  fprintf(f, "F                      - Print the flow so far\n");
  fprintf(f, "\n");
  fprintf(f, "AP how                 - Calculate and process the next augmenting path\n");
  fprintf(f, "MF how                 - Calculate the max flow\n");
  fprintf(f, "MC how                 - Calculate the minimum cut\n");
  fprintf(f, "\n");
  fprintf(f, "RESET                  - Reset the residual to the original graph\n");
  fprintf(f, "AO                     - Test the assignment overload\n");
  fprintf(f, "\n");
  fprintf(f, "?                      - Print commands.\n");
  fprintf(f, "Q                      - Quit.\n");
}

int main(int argc, char **argv)
{
  Graph *g, g2;

  istringstream ss;
  ifstream fin;
  ofstream fout;
  vector <string> sv;
  vector <double> v;
  string s, l;
  string prompt, cmd;
  string forc, how;
 
  forc = "FORC";
  how = "DME";

  if (argc > 2 || (argc == 2 && strcmp(argv[1], "--help") == 0)) {
    fprintf(stderr, "usage: bin/nf_tool [prompt]\n");
    fprintf(stderr, "\n");
    print_commands(stderr);
    exit(1);
  }

  g = new Graph;

  if (argc == 2) {
    prompt = argv[1];
    prompt += " ";
  }

  while (1) {
    try { 
      if (prompt != "") printf("%s", prompt.c_str());
      if (!getline(cin, l)) return 0;
      sv.clear();
      ss.clear();
      ss.str(l);
      while (ss >> s) sv.push_back(s);
  
      /* Basic commands. */
  
      if (sv.size() == 0) {
      } else if (sv[0][0] == '#') {
      } else if (sv[0] == "?") {
        print_commands(stdout);
      } else if (sv[0] == "Q") {
        exit(0);
      } else if (sv[0] == "R") {
        if (sv.size() != 2) throw SRE("usage: R file");
        fin.clear();
        fin.open(sv[1].c_str());
        if (fin.fail()) throw SRE("couldn't open " + sv[1]);
        g->Read(fin);
        fin.close();
      
      } else if (sv[0] == "PR") {
        g->Write(cout, 'R');
      } else if (sv[0] == "W" || sv[0] == "J") {
        if (sv.size() != 3 || sv[2].size() != 1) throw SRE("usage: " + sv[0] + " file FORC");
        if (forc.find(sv[2][0]) == string::npos) throw SRE("usage: " + sv[0] + " file FORC");
        if (sv[1] == "-") {
          if (sv[0] == "W") {
            g->Write(cout, sv[2][0]);
          } else {
            g->Jgraph(cout, sv[2][0]);
          }
        } else {
          fout.clear();
          fout.open(sv[1].c_str());
          if (fout.fail()) throw SRE("couldn't open " + sv[1]);
          if (sv[0] == "W") {
            g->Write(fout, sv[2][0]);
          } else {
            g->Jgraph(fout, sv[2][0]);
          }
          fout.close();
        }
      } else if (sv[0] == "F") {
        cout << g->Get_Flow() << endl;
      } else if (sv[0] == "P") {
        cout << g->Get_Num_Paths() << endl;
      } else if (sv[0] == "AP" || sv[0] == "MC" || sv[0] == "MF") {
        if (sv.size() != 2 || sv[1].size() != 1) throw SRE((string) "usage: " + sv[0] + " how");
        if (how.find(sv[1][0]) == string::npos) throw SRE((string) "usage: " + sv[0] + " how");
        if (sv[0] == "AP") {
          g->Do_Augmenting_Path(sv[1][0], true);
        } else if (sv[0] == "MF") {
          g->Calculate_Max_Flow(sv[1][0]);
        } else if (sv[0] == "MC") {
          g->Calculate_Min_Cut(sv[1][0]);
        }
      } else if (sv[0] == "RESET") {
        g->Reset();
      } else if (sv[0] == "AO") {
        g2 = *g;
        delete g;
        g = new Graph;
        *g = g2;
      } else {
        cout << "Bad command: " << sv[0] << endl;
      }
      
    } catch (const SRE &e) {
      cout << e.what() << endl;
    }
  }

  return 0;
}
