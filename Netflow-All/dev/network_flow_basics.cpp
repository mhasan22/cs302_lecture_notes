#include <cmath>
#include "network_flow.hpp"
#include <iostream>
using namespace std;

typedef runtime_error SRE;

namespace network_flow
{

void Graph::Clear()
{
  Source = -1;
  Sink = -1;
  Flow = 0;
  Num_Paths = 0;
  Min_Cut.clear();
  Nodes.clear();
  Edges.clear();
  N_Map.clear();
  E_Map.clear();
}

void Graph::Reset()
{
  size_t i;
  Edge *e;

  Num_Paths = 0;
  Flow = 0;
  Min_Cut.clear();
  for (i = 0; i < Nodes.size(); i++) Nodes[i].Residual.clear();
  for (i = 0; i < Edges.size(); i++) {
    e = &(Edges[i]);
    e->Flow = 0;
    e->Residual = e->Original;
    if (e->Original > 0) {
      e->Res_Index = Nodes[e->From].Residual.size();
      Nodes[e->From].Residual.push_back(e->Id);
    } else {
      e->Res_Index = -1;
    }
  }
}

int Graph::Get_Node_By_Name(const string &name)
{
  int index;

  if (N_Map.find(name) == N_Map.end()) {
    index = Nodes.size();
    N_Map[name] = index;
    Nodes.resize(index+1);
    Nodes[index].Name = name;
    Nodes[index].Id = index;
    Nodes[index].X = -1;
    Nodes[index].Y = -1;
  } else {
    index = N_Map[name];
  }
  return index;
}

int Graph::Create_Edge(const string &from, const string &to, int weight)
{
  int f, t;
  string key;
  int index;
  Edge *e;

  key = from + "->" + to;
  if (E_Map.find(key) != E_Map.end()) {
    if (weight > 0) throw SRE("Duplicate edge: " + key);
    throw SRE("Can't specify edge and reverse edge" + key);
  }
  f = Get_Node_By_Name(from);
  t = Get_Node_By_Name(to);
  index = Edges.size();
  Edges.resize(index+1);
  E_Map[key] = index;
  e = &(Edges[index]);
  e->Id = index;
  e->Name = key;
  e->From = f;
  e->To = t;
  e->Original = weight;
  e->Residual = weight;
  e->Flow = 0;
  if (weight > 0) {
    e->Res_Index = Nodes[f].Residual.size();
    Nodes[f].Residual.push_back(index);
  } else {
    e->Res_Index = -1;
  }
  return index;
}

void Graph::Create_Edge_And_Reverse(const string &from, const string &to, int weight)
{
  int index, rindex;

  index = Create_Edge(from, to, weight);
  rindex = Create_Edge(to, from, 0);
  Edges[index].Reverse_Id = rindex;
  Edges[rindex].Reverse_Id = index;
}

void Graph::Read(istream &fin)
{
  string s;
  string from, to;
  int weight;
  double x, y;
  int index;

  Clear();
  while (fin >> s) {
    if (s == "SOURCE") {
      if (!(fin >> s)) throw SRE("SOURCE without a node specified");
      Source = Get_Node_By_Name(s);
    } else if (s == "SINK") {
      if (!(fin >> s)) throw SRE("SINK without a node specified");
      Sink = Get_Node_By_Name(s);
    } else if (s == "EDGE") {
      if (!(fin >> from >> to >> weight)) throw SRE("EDGE needs from, to, weight");
      if (weight <= 0) throw SRE("EDGE weight must be > 0");
      Create_Edge_And_Reverse(from, to, weight);
    } else if (s == "LOC") {
      if (!(fin >> s >> x >> y)) throw SRE("LOC needs name, x, y");
      index = Get_Node_By_Name(s);
      Nodes[index].X = x;
      Nodes[index].Y = y;
    }  else {
      throw SRE("Unknown key in input: " + s);
    }
  }
  if (Source == -1) throw SRE("Source not defined");
  if (Sink == -1) throw SRE("Sink not defined");
}

void Graph::Jgraph(ostream &fout, char forc) const
{
  vector <double> X;
  vector <double> Y;
  size_t i;
  double min_x, min_y, max_x, max_y;
  const Edge *e;
  int v;
  double hyp, angle, pi, midx, midy;
  char buf[1000];

  pi = acos(-1.0);

  if (Nodes.size() == 0) {
    fout << "(* No Graph *)" << endl;
    return;
  }

  for (i = 0; i < Nodes.size(); i++) {
    if (Nodes[i].X == -1) {
      X.push_back(drand48()*10);
      Y.push_back(drand48()*10);
    } else {
      X.push_back(Nodes[i].X);
      Y.push_back(Nodes[i].Y);
    }
  }
  
  min_x = X[0];
  max_x = X[0];
  min_y = Y[0];
  max_y = Y[0];

  for (i = 0; i < X.size(); i++) {
    if (X[i] < min_x) min_x = X[i];
    if (X[i] > max_x) max_x = X[i];
    if (Y[i] < min_y) min_y = Y[i];
    if (Y[i] > max_y) max_y = Y[i];
  }
  min_x--;
  min_y--;
  max_x++;
  max_y++;

  fout << "newgraph\n";
  fout << "xaxis min " << min_x << " max " << max_x << " size 6 nodraw\n";
  fout << "yaxis min " << min_y << " max " << max_y << " size 6 nodraw\n";
  fout << "curve 0 marktype circle marksize .3 .3 fill 1 linetype solid rarrows\n";
  fout << "copycurve 0 linetype none\n";
  fout << "string 0 fontsize 10 hjc vjb\n";
  fout << "copystring fontsize 12 hjc vjc\n";

  if (forc == 'C') {
    for (i = 0; i < Min_Cut.size(); i++) {
      e = &(Edges[Min_Cut[i]]);
      fout << "copycurve 0 pts " << X[e->From] << " " << Y[e->From] 
                                 << " " << X[e->To] << " " << Y[e->To] << endl;
    }
  } else {
    for (i = 0; i < Edges.size(); i++) {
      e = &(Edges[i]);
      switch(forc) {
        case 'O': v = e->Original; break;
        case 'F': v = e->Flow; break;
        case 'R': v = e->Residual; break;
      }
      if (v > 0) {
        cout << pi << endl;
        hyp = sqrt( (X[e->From] -  X[e->To]) * (X[e->From] -  X[e->To]) + 
                    (Y[e->From] -  Y[e->To]) * (Y[e->From] -  Y[e->To]) );
        angle = acos((X[e->From] -  X[e->To])/hyp);
        // Fix this.
        if (angle > pi/2.0 && angle <= 3.0*pi/2) angle += pi;
        midx = (X[e->From] +  X[e->To]) / 2.0;
        midy = (Y[e->From] +  Y[e->To]) / 2.0;
        sprintf(buf, "copystring 0 x %lg y %lg rotate %lg : %d\n", midx, midy, angle*180.0/pi, v);
        fout << buf;

        if (forc == 'R') {
          fout << "copycurve 0 pts " << X[e->From] << " " << Y[e->From] 
                                     << " " << X[e->To] << " " << Y[e->To] << endl;
        } else {
          fout << "copycurve 0 pts " << X[e->From] << " " << Y[e->From] 
                                     << " " << X[e->To] << " " << Y[e->To] << endl;
        }
      }
    }
  }

  for (i = 0; i < Nodes.size(); i++) {
    fout << "copycurve 1 pts " << X[i] << " " << Y[i] << endl;
    fout << "copystring 1 x " << X[i] << " y " << Y[i] << " : " << Nodes[i].Name << endl;
  }
}

void Graph::Write(ostream &fout, char forc) const
{
  size_t i;
  const Edge *e = NULL;
  int w;
  char buf[20];

  if (Nodes.size() == 0) return;
  if (forc == 'C') {
    for (i = 0; i < Min_Cut.size(); i++) {
      e = &(Edges[Min_Cut[i]]);
      sprintf(buf, "%5d ", e->Original);
      fout << buf << e->Name << endl;
    }
  } else {
    if (forc == 'F') fout << "Flow: " << Flow << endl;
    fout << "Source: " << Nodes[Source].Name << endl;
    fout << "Sink: " << Nodes[Sink].Name << endl;
    for (i = 0; i < Edges.size(); i++) {
      e = &(Edges[i]);
      switch(forc) {
        case 'O': w = e->Original; break;
        case 'R': w = e->Residual; break;
        case 'F': w = e->Flow; break;
      } 
      if (w > 0) {
        sprintf(buf, "%5d ", w);
        fout << buf << e->Name << endl;
      }
    }
  }
}

bool Graph::DFS(int node, vector <bool> &visited, vector <const Edge *> &path) const
{
  size_t i;
  const Node *n;
  const Edge *e;

  if (visited[node]) return false;
  visited[node] = true;
  if (node == Sink) return true;
  n = &(Nodes[node]);
  for (i = 0; i < n->Residual.size(); i++) {
    e = &Edges[n->Residual[i]];
    if (DFS(e->To, visited, path)) {
      path.push_back(e);
      return true;
    }
  }
  return false;
}

bool Graph::Do_Augmenting_Path(char how, bool print)
{
  vector <const Edge *> path;
  vector <bool> visited;
  size_t i;
  int flow;
  Edge *e, *re, *last;
  Node *n;

  if (Nodes.size() == 0) return false;

  if (how == 'D') {
    visited.resize(Nodes.size(), false);
    DFS(Source, visited, path);
  }
  if (path.size() == 0) return false;

  flow = path[0]->Residual;
  for (i = 1; i < path.size(); i++) if (path[i]->Residual < flow) flow = path[i]->Residual;
  Flow += flow;
  Num_Paths++;

  if (print) {
    printf("%5d: ", flow);
    for (i = path.size(); i > 0; i--) printf("[%s]", path[i-1]->Name.c_str());
    printf("\n");
  }

  for (i = 0; i < path.size(); i++) {
    e = &(Edges[path[i]->Id]);
    re = &(Edges[path[i]->Reverse_Id]);
    e->Residual -= flow;
    if (e->Residual == 0) {
      n = &(Nodes[e->From]);
      last = &(Edges[n->Residual.back()]);
      last->Res_Index = e->Res_Index;
      n->Residual[last->Res_Index] = last->Id;
      n->Residual.pop_back();
    }
    re->Residual += flow;
    if (re->Residual == flow) {
      n = &(Nodes[re->From]);
      re->Res_Index = n->Residual.size();
      n->Residual.push_back(re->Id);
    }
  }
  
  return true;
}

void Graph::Calculate_Max_Flow(char how)
{ 
  while (Do_Augmenting_Path(how, false)) ;
}

void Graph::MC_DFS(int node, vector <bool> &visited)
{
  size_t i;

  if (visited[node]) return;
  visited[node] = true;
  for (i = 0; i < Nodes[node].Residual.size(); i++) MC_DFS(Nodes[node].Residual[i], visited); 
}

void Graph::Calculate_Min_Cut(char how)
{ 
  vector <bool> visited;
  size_t i;

  Min_Cut.clear();
  Calculate_Max_Flow(how);
  visited.resize(Nodes.size(), false);
  MC_DFS(Source, visited);
  for (i = 0; i < Edges.size(); i++) {
    if (visited[Edges[i].From] && !visited[Edges[i].To]) Min_Cut.push_back(i);
  }
}

int Graph::Get_Flow() const { return Flow; }
int Graph::Get_Num_Paths() const { return Num_Paths; }
};
