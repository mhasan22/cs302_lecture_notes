/* Read lines of text and add them as key/value objects in JSON.  Then print the JSON. */

#include "nlohmann/json.hpp"
#include <iostream>
#include <vector>
#include <sstream>
using namespace std;
using nlohmann::json;

int main()
{
  string key, val;
  double v;
  vector <string> sv;
  istringstream ss;
  string line;
  string s;
  size_t i;
  json js;

  /* Read in lines of text.  

     If there are two words, then store a key/value pair in the json.  If the value
        can be interpreted as a double, then store it as a double.  Otherwise, store
        it as a string.

     If there are more than two words, then have the value be an array.
   */

  js = json::object();

  /* Read the line and turn it into a vector of words. */

  while (getline(cin, line)) {
    sv.clear();
    ss.clear();
    ss.str(line);
    while (ss >> s) sv.push_back(s);

    /* Set js[key] to val, using sscanf to see if val is a double or string */

    if (sv.size() == 2) {   
      key = sv[0];
      if (sscanf(sv[1].c_str(), "%lf", &v) == 1) {   // It's a double.
        js[key] = v;
      } else {                                       // It's a string.
        val = sv[1];
        js[key] = val;
      }

    /* Otherwise set js[key] to be an array of values. */

    } else if (sv.size() > 2) {  
      key = sv[0];
      js[key] = json::array();
      for (i = 1; i < sv.size(); i++) {
        if (sscanf(sv[i].c_str(), "%lf", &v) == 1) {   // It's a double.
          js[key].push_back(v);
        } else {                                       // It's a string.
          js[key].push_back(sv[i]);
        }
      }
    }
  }
   
  /* Print it out. */
  
  cout << js << endl;
  return 0;
}
